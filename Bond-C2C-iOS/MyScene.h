//
//  MyScene.h
//  Bond-C2C-iOS
//

//  Copyright (c) 2014 Timothy Hart. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyScene : SKScene <UIGestureRecognizerDelegate>

@property (nonatomic) SKSpriteNode *bond;
@property (nonatomic, strong) NSArray *bondRunFrames;
@property (nonatomic, strong) NSArray *leftBondRunFrames;
@property int currentRunFrame;
@property (nonatomic, strong) NSArray *bondJumpFrames;
@property (nonatomic, strong) NSArray *leftBondJumpFrames;
@property int currentJumpFrame;
@property (nonatomic, strong) NSArray *bondCrouchFrames;
@property (nonatomic, strong) NSArray *leftBondCrouchFrames;
@property int currentCrouchFrame;
@property BOOL isBondCrouching;

@property BOOL isBondFacingLeft;

@property BOOL hasBeatenLevel;

@property (nonatomic) UISwipeGestureRecognizer *swipeUpGesture;
@property (nonatomic) UISwipeGestureRecognizer *swipeDownGesture;


-(void)moveBondToRightSideOfScreen;

@end
